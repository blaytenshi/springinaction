package autowiring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * Created by jtang on 5/10/16.
 */
@Configuration
@ComponentScan(basePackageClasses = {RoomConfig.class})
public class RoomConfig {

}
