package autowiring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by jtang on 5/10/16.
 */
public class RoomMain {
    public static void main(String args[]) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RoomConfig.class);

        Room room = (Room) context.getBean(Room.class);

        Table table = room.getTable();

        System.out.println(table.toString());

    }
}
