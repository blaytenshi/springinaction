package autowiring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jtang on 5/10/16.
 */
@Component
public class Room {

    private Table table;

    @Autowired
    public void setTable(Table table) {
        this.table = table;
    }

    public Table getTable() {
        return table;
    }
}
