package autowiring;

import org.springframework.stereotype.Component;

/**
 * Created by jtang on 5/10/16.
 */
@Component
public class Table {

    private String name;

    // If @ComponentScan is enabled in configuration and a class tagged with @Component, Spring will instantiate
    // a default bean from the empty constructor (this one).
    public Table() {
        // If the line below commented out, the getTable() called on Room bean in RoomMain.class will return null.
        this.name = "Table";
    }

    @Override
    public String toString() {
        return name;
    }
}
