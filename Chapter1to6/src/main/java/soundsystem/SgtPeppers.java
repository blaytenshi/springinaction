package soundsystem;

import org.springframework.stereotype.Component;

@Component // tells spring that this is a component that will be turned into a bean
public class SgtPeppers implements CompactDisc {

    private String title = "Sgt. Pepper's Lonely Hearts Club Band";
    private String artist = "The Beatles";

    @Override
    public void play() {
        // modified the output to print because the Test case does not like the new line produced by println
        System.out.print("Playing " + title + " by " + artist);
    }
}
