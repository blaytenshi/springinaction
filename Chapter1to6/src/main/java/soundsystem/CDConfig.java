package soundsystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;


@Configuration
public class CDConfig {
    @Bean
    public CompactDisc compactDisc() {

        // Removed the old SgtPeppers() instantiation and replaced it with a BlankDisc instantiation instead
        // This is so that the tests pass, using SgtPeppers() will cause it to not output the tracklist and thus fail
        BlankDisc sgtPeppers = new BlankDisc();
        sgtPeppers.setArtist("The Beatles");
        sgtPeppers.setTitle("Sgt. Pepper's Lonely Hearts Club Band");

        List<String> trackList = new ArrayList<String>();
        trackList.add("Sgt. Pepper's Lonely Hearts Club Band");
        trackList.add("With a Little Help from My Friends");
        trackList.add("Lucy in the Sky with Diamonds");
        trackList.add("Getting Better");
        trackList.add("Fixing a Hole");

        sgtPeppers.setTracks(trackList);
        return sgtPeppers;
    }
}
