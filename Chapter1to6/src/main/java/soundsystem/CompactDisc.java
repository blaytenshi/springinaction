package soundsystem;

public interface CompactDisc {
    void play(); // everything that is a compact disc needs to implement this method
}
