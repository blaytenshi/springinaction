package soundsystem;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CDPlayer implements MediaPlayer {
    private CompactDisc cd;

    // Adding an empty constructor for CDPlayerConfig to be able to instantiate a bean.
    // Let the autowiring from CDPlayerTest wire in the necessary CompactDisc bean
    public CDPlayer() {

    }

    // Adding Constructor back in for constructor injection so CDPlayerConfig class can work
    public CDPlayer(CompactDisc compactDisc) {
        this.cd = compactDisc;
    }

    // Removed constructor to use property setter method injection instead.
    // Use constructor injection for hard dependencies (title, artist, tracklist are hard dependencies for BlankDisc, doens't work without them)
    // Use property injection for optional dependencies (cd can be a soft dependency for CDPlayer because the CDPlayer can play, say, radio)
    @Autowired
    public void setCompactDisc(CompactDisc cd) {
        this.cd = cd;
    }

    public void play() {
        cd.play();
    }
}