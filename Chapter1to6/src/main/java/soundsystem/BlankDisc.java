package soundsystem;

import java.util.List;

public class BlankDisc implements CompactDisc {
    private String title;
    private String artist;
    // Adding List<String>s to BlankDisc's fields
    private List<String> tracks;

    // Removing constructor in favour of literal setter properties injection instead
    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setTracks(List<String> tracks) {
        this.tracks = tracks;
    }

    // Ensuring the play() prints out the list of tracks
    public void play() {
        System.out.print("Playing " + title + " by " + artist);
        // Will not print tracks if tracks is null or is empty. Added to make sure tests pass.
        if (tracks != null && !tracks.isEmpty()) {
            for (String track : tracks) {
                System.out.print("-Track: " + track);
            }
        }
    }

}
