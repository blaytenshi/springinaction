package soundsystem;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@Configuration
// Removed CDConfig class from @Import array
@Import({CDPlayerConfig.class})
// Added the CDConfig xml (equivalent to the CDConfig class
@ImportResource("classpath:cd-config.xml")
public class SoundSystemConfig {
    // New config file used to pull together CDPlayerConfig.class and CDConfig.class with @Import annotation
}
