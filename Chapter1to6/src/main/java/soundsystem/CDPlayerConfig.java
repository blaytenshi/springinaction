package soundsystem;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
// Removed import from CDPlayerConfig and into SoundSystemConfig class
public class CDPlayerConfig {

    // Returning this to an empty constructor bean instantiation because we cannot reference the CD Bean from here.
    // Autowiring in the tests will take care of wiring the CD bean into this CDPlayer
    @Bean
    public CDPlayer cdPlayer() {
        return new CDPlayer();
    }

}
