package soundSystem2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;

/**
 * Created by jtang on 5/10/16.
 */
@Configuration
@PropertySource("classpath:/app.properties")
public class ExpressiveConfig {

    @Autowired
    Environment env;

    @Value("${disc.title}")
    public String title;

    @Value("${disc.artist}")
    public String artist;

    @Value("${disc.trackNumber}")
    public int trackNumber;

    @Bean
    public BlankDisc disc() {
        return new BlankDisc(title, artist, trackNumber);
    }

    // This bean is required for using property placeholders in your property injections
    @Bean
    public static PropertySourcesPlaceholderConfigurer placeholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}