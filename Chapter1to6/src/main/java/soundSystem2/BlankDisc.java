package soundSystem2;

import org.springframework.stereotype.Component;

/**
 * Created by jtang on 5/10/16.
 */
@Component
public class BlankDisc {

    private final String title;
    private final String artist;
    private int trackNumber;

    public BlankDisc(String title, String artist, int trackNumber) {
        this.title = title;
        this.artist = artist;
        this.trackNumber = trackNumber;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public int getTrackNumber() {
        return trackNumber;
    }
}
