package com.springinaction.knights;

// This class is more loosely coupled compared to DamselRescuingKnight! Stuff is injected!

public class BraveKnight implements Knight {
    private Quest quest; // Quest is an interface so we can have all kinds of quests... RoundTableQuest? Sure!

    public BraveKnight(Quest quest) { // doesn't create his own quest
        this.quest = quest;           // instead, braveKnight is given a quest at construction time
    }                                 // this is what's known as Dependency Injection (DI)

    public void embarkOnQuest() {
        quest.embark();
    }

}
