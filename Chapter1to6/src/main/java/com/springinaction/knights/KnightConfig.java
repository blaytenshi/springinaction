package com.springinaction.knights;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KnightConfig {
    @Bean
    public Knight knight() {
        return new BraveKnight(quest()); // The "Quest" object is injected by calling the quest() method below. That returns
    }                                    // a Quest object to be injected into the newly instantiated BraveKnight object.

    @Bean
    public Quest quest() {
        return new SlayDragonQuest(System.out);
    }
}
