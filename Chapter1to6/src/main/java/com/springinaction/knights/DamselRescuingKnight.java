package com.springinaction.knights;

// This class is bad because everything is tightly coupled!
// DamselRescuingKnight can only do RescueDamselQuest!

public class DamselRescuingKnight implements Knight {
    private RescueDamselQuest quest;

    public DamselRescuingKnight() {
        this.quest = new RescueDamselQuest();
    }

    public void embarkOnQuest() {
        quest.embark();
    }

}
