package com.springinaction.knights;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class KnightMain {
    public static void main(String[] args) throws Exception {

        // Use this below if your'e using the XML config
        // If you're using XML config, you need to put it in java/resources folder!
        //ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("knight.xml");

        // Use this below if you're using the Java Config
        // You don't need to move the configuration class to java/resources because it is treated as a java class file...
        // We'll use Java config as the default because it's the way forward!
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(KnightConfig.class);

        Knight knight = context.getBean(Knight.class);
        knight.embarkOnQuest();
        context.close();
    }
}
