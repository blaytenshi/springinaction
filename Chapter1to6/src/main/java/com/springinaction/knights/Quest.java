package com.springinaction.knights;

/**
 * Created by blay.tenshi on 8/09/2016.
 */
public interface Quest {
    void embark();
}
