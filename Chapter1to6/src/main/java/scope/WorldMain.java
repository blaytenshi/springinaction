package scope;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by blaytenshi on 5/10/2016.
 */
public class WorldMain {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(WorldConfig.class);

        Robot robotRobbie = (Robot) context.getBean("robot");
        Robot robotC3PO = (Robot) context.getBean("robot");

        Person personJohn = (Person) context.getBean("person");
        Person personJohn2 = (Person) context.getBean("person");

        robotRobbie.setName("Robbie");
        robotC3PO.setName("C3PO");

        personJohn.setName("John");
        personJohn2.setName("Michael");

        System.out.println(robotRobbie.getName());  // will say Robbie
        System.out.println(robotC3PO.getName());    // will say C3PO

        System.out.println(personJohn.getName());   // will now say Michael
        System.out.println(personJohn2.getName());  // will also now say Michael

    }

}
