package scope;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * Created by blaytenshi on 5/10/2016.
 */
@ComponentScan(basePackageClasses = {WorldConfig.class})
@Configuration
public class WorldConfig {

    @Bean(name="robot")
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Robot getRobot() {
        return new Robot();
    }

    @Bean(name="person")
    @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
    public Person getPerson() {
        return new Person();
    }

}
