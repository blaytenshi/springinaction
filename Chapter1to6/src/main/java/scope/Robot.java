package scope;

/**
 * Created by blaytenshi on 5/10/2016.
 */
public class Robot {
    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
