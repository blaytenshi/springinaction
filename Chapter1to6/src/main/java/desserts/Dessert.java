package desserts;

public interface Dessert {
    String getDessertName();
}
