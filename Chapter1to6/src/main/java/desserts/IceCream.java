package desserts;

import org.springframework.stereotype.Component;

/**
 * Created by jtang on 30/9/16.
 */
// By default, the bean initialised from this class will be given a default
// Qualifier id of the uncapitilised class name (in this case "iceCream")
// As you can see, the commented out @Qualifier annotation is implicit
@Component
@Cold
public class IceCream implements Dessert {

    private String dessertName = "Ice Cream";

    @Override
    public String getDessertName() {
        return dessertName;
    }
}
