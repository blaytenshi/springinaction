package desserts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
// Removed all @Beans and replaced them with @ComponentScan
@ComponentScan(basePackageClasses = IceCream.class)
public class DessertsConfig {

    private Dessert dessert;

    // Autowiring (injecting) objects
    // The custom annotation ensures that you can have multiple Qualifier types
    // Java 8 also allows more than one Qualifier type but must be annotated with
    // @Repeatable annotation.
    @Autowired
    @Cold
    public void setDessert(Dessert dessert) {
        this.dessert = dessert;
    }

    // Creating Bean naming it dessert for use in DessertMain
    @Bean(name="dessert")
    public Dessert getDessert() {
        return dessert;
    }
}
