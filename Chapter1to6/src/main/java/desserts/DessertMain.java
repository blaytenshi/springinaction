package desserts;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Created by jtang on 30/9/16.
 */
public class DessertMain {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(DessertsConfig.class);

        Dessert dessert = (Dessert) context.getBean("dessert");

        System.out.println(dessert.getDessertName());

        context.close();
    }
}
