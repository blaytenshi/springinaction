package desserts;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by jtang on 30/9/16.
 */
@Component
@Qualifier("soft")
public class Cake implements Dessert {

    private String dessertName = "Cake";

    @Override
    public String getDessertName() {
        return dessertName;
    }
}
