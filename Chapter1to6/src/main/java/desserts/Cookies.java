package desserts;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by jtang on 30/9/16.
 */
@Component
@Qualifier("crispy")
public class Cookies implements Dessert {

    private String dessertName = "Cookies";

    @Override
    public String getDessertName() {
        return dessertName;
    }
}
