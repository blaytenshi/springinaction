package soundsystem;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.StandardOutputStreamLog;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
@RunWith(SpringJUnit4ClassRunner.class)
// The 'locations' attribute is used to specify the xml files
// The 'classes' attribute is used to specify an array of class files to use as config files
// Changed to soundsystemconfig.xml to test with for mixed Java and XML config
@ContextConfiguration(locations={"classpath:soundsystemconfig.xml"})
public class CDPlayerTest {

    @Rule
    public final StandardOutputStreamLog log = new StandardOutputStreamLog();

    @Autowired
    private MediaPlayer player;

    @Autowired
    private CompactDisc cd;

    @Test
    public void cdShouldNotBeNull() {
        assertNotNull(cd);
    }

    @Test
    public void play() {
        player.play();
        assertEquals(
                "Playing Sgt. Pepper's Lonely Hearts Club Band by The Beatles" +
                // Updating test case with tracks list
                "-Track: Sgt. Pepper's Lonely Hearts Club Band" +
                "-Track: With a Little Help from My Friends" +
                "-Track: Lucy in the Sky with Diamonds-Track: Getting Better" +
                "-Track: Fixing a Hole", log.getLog()
        );
    }

}
