package soundSystem2;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by jtang on 5/10/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:expressive-config.xml"})
public class ExpressiveTest {

    @Autowired
    private BlankDisc beatlesDisc;

    @Test
    public void discArtistMatches() {
        assertEquals("Artists do not match.", "The Beatles", beatlesDisc.getArtist());
    }

    @Test
    public void discTitleMatches() {
        assertEquals("Titles do not match.", "Sgt. Peppers Lonely Hearts Club Band", beatlesDisc.getTitle());
    }

    @Test
    public void discTrackNumberMatches() {
        assertEquals("Track Numbers do not match.", 5, beatlesDisc.getTrackNumber());
    }

}
