package com.springinaction.knights;
import static org.mockito.Mockito.*;
import org.junit.Test;

/**
 * Created by blay.tenshi on 8/09/2016.
 */
public class BraveKnightTest {

    @Test
    public void knightShouldEmbarkOnQuest() {
        Quest mockQuest = mock(Quest.class); // use mockito to create a mock Quest object
        BraveKnight knight = new BraveKnight(mockQuest); // inject created mock Quest object into BraveKnight
        knight.embarkOnQuest(); // call embarkOnQuest()
        verify(mockQuest, times(1)).embark(); // use mockito to ensure that it has been called once
    }

}
