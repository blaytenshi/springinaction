package desserts;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by jtang on 2/10/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={DessertsConfig.class})
public class DessertsTest {

    @Autowired
    @Cold
    private Dessert dessert;

    @Test
    public void dessertShouldNotBeNull() {
        assertNotNull(dessert);
    }

    @Test
    public void dessertPrimaryIsIceCream() {
        assertEquals("Dessert name is not Ice Cream!", "Ice Cream", dessert.getDessertName());
    }
}