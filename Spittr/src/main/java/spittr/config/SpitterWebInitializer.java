package spittr.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import spittr.web.WebConfig;

// This is the web initializer class.
// It extends the DispatcherServlet class and is used in place of the web.xml nowadays thanks to advancements in Servlet 3 specification and Spring 3.1
// To understand it:
// Servlet 3.0 environment
//     |
//     | Finds in classpath anything that implements
//     | - javax.servlet.ServletContainerInitializer
//     | which is used to figure the servlet container
//     |
//     V
// SpringServletContainerInitializer [implements ServletContainerInitializer]
//     |
//     | Like the Servlet 3.0 environment, finds in classpath anything that implements
//     | - WebApplicationInitializer
//     | which will be used for configuring the DispatcherServlet
//     |
//     V
// AbstractAnnotationConfigDispatcherServletInitializer [implements WebApplicationInitializer]
//     | Extended by
//     | - SpitterWebInitializer

public class SpitterWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    // Under the covers, AbstractAnnotationConfigDispatcherServletInitializer actually creates both a DispatchServlet and
    // ContextLoaderListener. Each with their own application context.

    // This method override asks the ContextLoaderListener to load its application context with beans defined in RootConfig.class
    // The beans defined in RootConfig.class usually contains middle-tier and data-tier components (repositories, data beans)
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {
                RootConfig.class
        };
    }

    // This method override asks DispatcherServlet to load its application context with beans defined in WebConfig.class
    // The beans defined in WebConfig.class usually contains web components such as controllers, view resolvers, and
    // handler mappings.
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {
                WebConfig.class
        };
    }

    // This method override is to identify the one or more paths that DispatcherServlet will be mapped to.
    // Here is is mapped to "/" indicating that it will be the application's default servlet
    @Override
    protected String[] getServletMappings() {
        return new String[] {
                "/"
        };
    }
}
