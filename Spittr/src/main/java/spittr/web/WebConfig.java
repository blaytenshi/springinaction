package spittr.web;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("spittr.web")
public class WebConfig extends WebMvcConfigurerAdapter {

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        // Added to allow JSTL formatting tags to be used because they require Locale to properly
        resolver.setViewClass(org.springframework.web.servlet.view.JstlView.class);
        return resolver;
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        super.addResourceHandlers(registry);
    }

    // Creates a messageSource to retrieve messages from a properties file in the file system.
    // This messageSource can be used in the spring tagged jsp pages
    @Bean
    public MessageSource messageSource() {
        // You can use ResourceBundleMessageSource but ReloadableResourceBundleMessageSource allows you to reload message properties without recompiling the application
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        // this sets the location of the messages properties file
        // prefix with classpath:// if you're looking for the message file in the resource folder
        // prefix with file:// if you're loading the message file from a specific location in the system
        // no prefix if you're loading the message file from the root directory of the application
        messageSource.setBasename("classpath:messages");
        //messageSource.setBasename("file:///Users/jtang/Code/springinaction/Spittr/src/main/resources/messages");
        //messageSource.setBasename("messages"); // Looks for the messages.properties in the root of the webapps folder

        messageSource.setCacheSeconds(10);
        return messageSource;
    }
}
