package spittr.data;

import spittr.Spitter;

/**
 * Created by blaytenshi on 12/10/2016.
 */
public interface SpitterRepository {
    Spitter save(Spitter spitter);
    Spitter findByUsername(String username);
}
