<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ page session="false" %>
<html>
  <head>
    <title>Spitter</title>
    <link rel="stylesheet" 
          type="text/css" 
          href="<c:url value="/resources/style.css" />" >
  </head>
  <body>
    <h1><s:message code="spittr.welcome" /></h1>
    <a href="<c:url value="/spittles" />">Spittles</a> |

    <s:url value="/spitter/register" var="registerUrl"/>
    <a href="${registerUrl}">Register</a>

    <%-- ESCAPED CONTENT --%>
    <%--
    If you want to display a snipper of HTML code on a page, for it to display properly you need to replace
    characters like '<' and '>' with &lt; and &gt; or it'll be interpreted as regular tags by the browser.
    The <s:escapeBody> tag can help with that.
    --%>
    <s:escapeBody htmlEscape="true">
        <h1>Test Escaped Text!</h1>
    </s:escapeBody>
    <%-- The above will be rendered as &lt;h1&gt;Hello&lt;/h1&gt; on the page --%>
  </body>
</html>
