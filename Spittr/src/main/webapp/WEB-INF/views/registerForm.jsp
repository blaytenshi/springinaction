<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
    <head>
        <title>Spittr</title>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/style.css" />" >
    </head>
    <body>
        <h1>Register</h1>
        <sf:form method="POST" commandName="spitter">
            <!-- setting path attribute to value * means it will list all errors -->
            <!-- cssClass attribute is the value that the class attribute will be set to -->
            <!-- sf:errors defaults to a span tag. Element changes it to a div tag instead -->
            <sf:errors cssClass="errors" element="div" path="*" />
            <sf:label path="firstName" cssErrorClass="error">First Name</sf:label>: <sf:input path="firstName" cssErrorClass="error" /><br/>
            <sf:label path="lastName" cssErrorClass="error">Last Name</sf:label>: <sf:input path="lastName" cssErrorClass="error" /><br/>
            <sf:label path="email" cssErrorClass="error">Email</sf:label>: <sf:input path="email" cssErrorClass="error" /><br/>
            <sf:label path="username" cssErrorClass="error">Username</sf:label>: <sf:input path="username" cssErrorClass="error" /><br/>
            <sf:label path="password" cssErrorClass="error">Password</sf:label>: <sf:password path="password" cssErrorClass="error" /><br/>
            <input type="submit" value="Register" /><br/>
        </sf:form>
    </body>
</html>